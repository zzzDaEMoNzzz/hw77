const express = require('express');
const nanoid = require('nanoid');
const db = require('../fileDB');
const multer  = require('multer');
const path = require('path');
const config = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.uploadPath);
  },
  filename: function (req, file, cb) {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', (req, res) => {
  const messages = db.getItems();
  const messagesToShow = 30;

  res.send(messages.slice(0 - messagesToShow));
});

router.post('/', upload.single('image'), (req, res) => {
  if (!req.body.message) {
    res.status(400).send({error: "Message must be present in the request"});
  } else {
    const message = {
      id: nanoid(),
      datetime: new Date().toISOString(),
      ...req.body,
    };

    if (req.file) {
      message.image = req.file.filename;
    }

    db.addItem(message);
    res.send({message: 'OK'});
  }
});

module.exports = router;
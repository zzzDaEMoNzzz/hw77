const express = require('express');
const cors = require('cors');
const db = require('./fileDB');
const messages = require('./app/messages');

db.init();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use('/messages', messages);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});
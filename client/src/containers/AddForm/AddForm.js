import React, {Component} from 'react';
import {connect} from "react-redux";
import './AddForm.css';
import {addMessage} from "../../store/actions";

class AddForm extends Component {
  state = {
    message: '',
    author: '',
    image: null,
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      if (this.state[key]) formData.append(key, this.state[key]);
    });

    this.props.addMessage(formData).then(() => {
      this.setState({
        message: '',
        image: null,
      });
    });
  };

  render() {
    return (
      <form className="AddForm" onSubmit={this.submitFormHandler}>
        <textarea
          className="AddForm-message"
          required
          name="message"
          value={this.state.message}
          onChange={this.inputChangeHandler}
          placeholder="Description"
        />
        <div className="AddForm-options">
          <label>Author</label>
          <input
            className="AddForm-author"
            type="text"
            name="author"
            value={this.state.author}
            onChange={this.inputChangeHandler}
          />
          <label>Image</label>
          <input
            className="AddForm-file"
            type="file"
            name="image"
            onChange={this.fileChangeHandler}
          />
          <button className="AddForm-sendBtn">Send</button>
        </div>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addMessage: data => dispatch(addMessage(data))
});

export default connect(null, mapDispatchToProps)(AddForm);
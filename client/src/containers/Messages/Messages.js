import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchMessages} from "../../store/actions";
import moment from 'moment';
import Message from "../../components/Message/Message";
import AddForm from "../AddForm/AddForm";
import './Messages.css';

class Messages extends Component {
  componentDidMount() {
    this.props.getMessages();
  }

  render() {
    const messages = this.props.messages.map(message => (
      <Message
        key={message.id}
        message={message.message}
        date={moment(message.datetime).format('DD/MM/YY ddd HH:mm:ss')}
        author={message.author}
        image={message.image}
      />
    ));

    console.log(this.props.messages);

    return (
      <div className="Messages">
        <div className="Messages-list">{messages}</div>
        <AddForm/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages
});

const mapDispatchToProps = dispatch => ({
  getMessages: () => dispatch(fetchMessages())
});

export default connect(mapStateToProps, mapDispatchToProps)(Messages);
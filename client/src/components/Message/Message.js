import React from 'react';
import ModalImage from 'react-modal-image'
import {apiURL} from "../../constants";
import './Message.css';

const Message = ({message, date, author, image}) => {
  console.log(!!image);

  const imageIsAvaliable = image ? (
    <div className="Message-image">
      <ModalImage
        small={`${apiURL}/uploads/${image}`}
        large={`${apiURL}/uploads/${image}`}
        alt=""
        hideDownload
        hideZoom
      />
    </div>
  ) : null;

  return (
    <div className="Message">
      <div className="Message-header">
        <span style={{
          fontStyle: author ? 'normal' : 'italic',
          color: author ? '#000' : '#555'
        }}>{author || 'anonym'}</span>
        <span>{date}</span>
      </div>
      <div className="Message-body">
        {imageIsAvaliable}
        <p>{message}</p>
      </div>
    </div>
  );
};

export default Message;

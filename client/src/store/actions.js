import axios from 'axios';

export const FETCH_MESSAGE_SUCCESS = 'FETCH_MESSAGE_SUCCESS';

export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGE_SUCCESS, messages});

export const fetchMessages = () => {
  return dispatch => {
    return axios.get('/messages').then(
      response => dispatch(fetchMessagesSuccess(response.data))
    );
  };
};

export const addMessage = data => {
  return dispatch => {
    return axios.post('/messages', data).then(
      () => dispatch(fetchMessages())
    );
  };
};